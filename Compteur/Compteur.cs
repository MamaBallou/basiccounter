﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compteur
{
    
    public class Compteur
    {
        private static int nb_compt;
        public Compteur()
        {
            Compteur.nb_compt = 0;
        }

        public static int incrementation()
        {
            Compteur.nb_compt++;
            return Compteur.nb_compt;
        }
        public static int decrementation()
        {
            if (Compteur.nb_compt == 0)
            {
                Compteur.nb_compt = 0;
            } else Compteur.nb_compt--;
            return Compteur.nb_compt;
        }
        public static int raz()
        {
            Compteur.nb_compt = 0;
            return Compteur.nb_compt;
        }
    }
}
