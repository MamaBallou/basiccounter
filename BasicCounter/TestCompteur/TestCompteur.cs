﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestCompteur
{
    [TestClass]
    public class TestCompteur
    {
        [TestMethod]
        public void TestIncrementation()
        {
            Compteur.Compteur compteur = new Compteur.Compteur();
            Compteur.Compteur.nb_compt = 2;
            Assert.AreEqual(3, Compteur.Compteur.incrementation());
            Assert.AreNotEqual(5, Compteur.Compteur.decrementation());
        }
        [TestMethod]
        public void TestDecrementation()
        {
            Compteur.Compteur compteur = new Compteur.Compteur();
            Compteur.Compteur.nb_compt = 4;
            Assert.AreEqual(3, Compteur.Compteur.decrementation());
            Assert.AreNotEqual(5, Compteur.Compteur.decrementation());
            Compteur.Compteur.nb_compt = 0;
            Assert.AreEqual(0, Compteur.Compteur.decrementation());
            Assert.AreNotEqual(3, Compteur.Compteur.decrementation());
        }
        [TestMethod]
        public void TestRaz()
        {
            Compteur.Compteur compteur = new Compteur.Compteur();
            Compteur.Compteur.nb_compt = 4;
            Assert.AreEqual(0, Compteur.Compteur.raz());
            Assert.AreNotEqual(3, Compteur.Compteur.decrementation());
        }
    }
}
